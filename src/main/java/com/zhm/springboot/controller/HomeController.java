package com.zhm.springboot.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by haiming.zhuang on 2016/7/11.
 */
@Controller
public class HomeController {
    @RequestMapping("/")
    public String root(){
        return "redirect:/user/list";
    }
    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String loginpage(){
        return "login";
    }
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        request.getSession().removeAttribute("currUser");
        return "redirect:/";
    }
    @RequestMapping("/nolimit")
    public String nolimit(HttpServletRequest request,HttpServletResponse respose) throws IOException {
        if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) {
            respose.setContentType("text/html; utf-8");
            respose.getWriter().write("无权限操作");
            return null;
        }else{
            return "nolimit";
        }
    }
}
