package com.zhm.springboot.dao.impl;

import com.zhm.springboot.dao.UserRoleDao;
import com.zhm.springboot.domain.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by haiming.zhuang on 2016/7/13.
 */
@Repository("userRoleDao")
public class UserRoleDaoImpl implements UserRoleDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final class UserRoleRowMapper implements RowMapper<UserRole>{

        @Override
        public UserRole mapRow(ResultSet rs, int i) throws SQLException {
            UserRole result = new UserRole();
            result.setEntry_date(rs.getTimestamp("entry_date"));
            result.setId(rs.getInt("id"));
            result.setRoleid(rs.getInt("roleid"));
            result.setUserid(rs.getInt("userid"));
            return result;
        }
    }
}
