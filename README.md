#security-demo
1、导入maven工程。

2、修改/src/resources/application.properties,改掉mysql数据库的连接信息。

3、本地启动redis，然后运行/src/java/com/zhm/Application.java

4、访问http://localhost:8090/

#权限列表
| 用户      | 查询  | 新增  | 修改  | 删除  |
| -------: |:------:|:------:|:------:|:------:|
| 总裁      |   √    |   √    |   √    |   √    |
| 总监      |   √    |   √    |   √    |         |
| 经理      |   √    |   √    |         |         |
| 员工      |   √    |         |         |         |
